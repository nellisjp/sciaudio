-------------------------------
 Features
-------------------------------
- Playback of WAV and MP3 files
- Unlimited number of sounds playing simultaneously\
- Fade in/out, looping and volume control
- Classification of sounds for playback management
- Multiple commands may be issued simultaneously to the 'controller' file
- Multiple controller files to avoid resource contention
- Runs hidden in background & will terminate itself shortly upon game close
- Calls to sciAudio are performed very similarly to the built-in SCI sound calls 
- Poor man's encryption (MP3's only) - simply rename your .MP3 to be .sciAudio

-------------------------------
 Limitations
-------------------------------
 Works only in Windows (requires .NET framework)

-------------------------------
 Setup
-------------------------------
1) Place the 2 nAudio DLLs & executable into a subfolder within your game named 'sciAudio'
2) Create subfolders within sciAudio for your playback files.  Something like this
       <gamedir>/sciAudio/effects
	   <gamedir>/sciAudio/music
3) Include the sciAudio.sc script & 'use' it in any script you wish to have sciAudio playback in
	   
-------------------------------
 Available commands
-------------------------------
	command <playback command>
		play
		  Begins playback of a sound

		stop
		  Stops playback of a sound

		change
		  Changes playback of a sound.  Used primarily for volume & loop control, very limited usefulness.

		playx (play exclusively based on sound class)
		  This behaves just like the 'play' command, but will stop any currently playing sounds with
		  the same sound class as the specified class.	
			
	fileName 
	    Filename to playback, including the path.
		
	soundClass
	    Sound class to assign this sound.  This is simply a string to assign to the playback
		for the purpose of potentially changing or terminating all sounds with the same 
		sound class at a later time.
	
	    For example, you might have several sound effects playing simultaneously in a room.
		Upon leaving the room, you want to stop all the sound effects.  To to this you would
		issue a stop <soundclass> command, which would stop all currently playing sounds with
		the specified sound class.
		
		If no sound class is specified, a default sound class of "noClass" will be used 
	
	volume
		Playback volume of sample.  Default is 100 (100% volume of sample).  Range: 0 to ~300?
		
	fadeInMillisecs
	    Number of milliseconds to fade in the sample
		
	fadeOutMillisecs
	    Number of milliseconds to fade out a sample.  Can be issued with a play or stop command
		
	loopFadeInMillisecs
	    Number of milliseconds to fade in a sample between loops.  
		
	loopFadeOutMillisecs
	    Number of milliseconds to fade out a sample between loops.
		
	loopCount
	    Number of times to loop the sample.  Default is 0, -1 is infinite
		
	conductorFile
	    'Conductor' file name used to place commands into.  This is how commands are passed between 
		the SCI virtual machine & the sciAudio application.  The sciAudio application constantly polls all
		conductor files, looking for changes & executes them.  These files must have extension of .con. 
		
		The default value for this parameter is command.con.
		
		Utilization of this parameter is useful in the event you have many near-simultaneous playback 
		commands issued from within your game.  Multiple different conductor files can mitigate the 
		issue of potential file contention of just using a single file.  Most game developers will 
		probably not need to use this option.

	
	playXFadeOutMillisecs 
	    Only used for the 'playx' command.  Upon terminating any currently playing sounds for a
		sound class, this value will be used for the fade out.

-------------------------------
 Examples
-------------------------------
(use "sciAudio")

(local
    snd
)

(instance aud of sciAudio
   (properties)
   (method (init)
      (super:init())
   )
)

...

= snd aud

// basic playback
(send snd:
	command("play")
	fileName("effects\\score.sciAudio")
	volume("35")
	loopCount("0")
	fadeInMillisecs("3000")   // fade in beginning by 3 secs
	fadeOutMillisecs("2000")  // fade out ending by 2 secs
	init()
)

// looping playback example
(send snd:
    command("play")
    fileName("effects\\loopedSound.sciAudio")
    volume("100")
    loopFadeInMillisecs("2000")
    loopFadeOutMillisecs("2000")
    loopCount("-1")  // loop forever
    init()
)

// stop with 5 second fade out
(send snd:
	command("stop")
	fileName("music\\introMusic.sciAudio")
	fadeOutMillisecs("5000")
	loopCount("0")
	init()
)

// change currently playing volume for a sound class
(send snd:
	command("change")
	soundClass("myMusicSoundClass")
	volume("50")   // set playback to 50% volume
	init()
)

// stop looping a particular sound file
(send snd:
	command("change")
	soundClass("music\\introMusic.sciAudio")
	loopCount("0")  // stop looping
	init()
)

// exclusive playback (starts new playback of sound 
// & stops any already playing sounds (with fadeout) for same soundClass
(send snd:
	command("playx")
	soundClass("narration")
	conductorFile("sciAudio\\narrate.con")
	fileName("sciAudio\\newNarration.con")
	playXFadeOutMillisecs("1500") // will fade currently playing "narration" sound class sounds
	volume("200")
  init()
)

-------------------------------
 Notes
-------------------------------
If you decide to re-use the same sound instance for multiple sounds, it's up to you
to reset any previously specified properties, otherwise they will be carried over 
into the next command