﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NAudio.Wave;
using NAudio.Wave.SampleProviders;
using System.Threading;
using System.IO;
using System.Diagnostics;
using System.Net;
using System.Runtime.InteropServices;
using System.Timers;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace sciAudio
{

    class IniFile   // revision 11
    {
        string Path;
        string EXE = Assembly.GetExecutingAssembly().GetName().Name;

        [DllImport("kernel32", CharSet = CharSet.Unicode)]
        static extern long WritePrivateProfileString(string Section, string Key, string Value, string FilePath);

        [DllImport("kernel32", CharSet = CharSet.Unicode)]
        static extern int GetPrivateProfileString(string Section, string Key, string Default, StringBuilder RetVal, int Size, string FilePath);

        public IniFile(string IniPath = null)
        {
            Path = new FileInfo(IniPath ?? EXE + ".ini").FullName;
        }

        public string Read(string Key, string Section = null)
        {
            var RetVal = new StringBuilder(255);
            GetPrivateProfileString(Section ?? EXE, Key, "", RetVal, 255, Path);
            return RetVal.ToString();
        }

        public void Write(string Key, string Value, string Section = null)
        {
            WritePrivateProfileString(Section ?? EXE, Key, Value, Path);
        }

        public void DeleteKey(string Key, string Section = null)
        {
            Write(Key, null, Section ?? EXE);
        }

        public void DeleteSection(string Section = null)
        {
            Write(null, null, Section ?? EXE);
        }

        public bool KeyExists(string Key, string Section = null)
        {
            return Read(Key, Section).Length > 0;
        }
    }

    public static class Utils
    {
        public static int gameVolume = 15;  // SCI game volume, 0-15 0 = 0%, 15 = 100% - init to full volume

        public static float computeVolume(float sampleVolume)
        {
            return (sampleVolume * ((float)gameVolume / (float)(15)) / (float)100.00 );
        }
    }


    public class AudioAttr
    {
        public string command;      // play, stop, volchange
        public string fileName;  
        public string soundClass;
        public int volume;          // computed volume, based on game volume & sample volume
        public float sampleVolume;  // 0-300 (0 = 0%, 100 = 100%, 300 = 300%)
        public int fadeInMillSecs;
        public int fadeOutMillSecs;
        public int loopFadeInMillSecs;
        public int loopFadeOutMillSecs;
        public int loopCount;      

        
        public AudioAttr()
        {
          soundClass = "noClass";
          volume = 100;
          fadeInMillSecs = 0;
          fadeOutMillSecs = 0;
          loopFadeInMillSecs = 0;
          loopFadeOutMillSecs = 0;
          loopCount = 0;
        }
    }

 
    public class AudioFileReader : WaveStream, ISampleProvider 
    {

        enum FadeState
        {
            Silence,
            FadingIn,
            FullVolume,
            FadingOut
        }

        private string fileName;
        private string soundClass;

        private WaveStream readerStream; // the waveStream which we will use for all positioning
        private SampleChannel sampleChannel; // sample provider that gives us most stuff we need
        private int destBytesPerSample;
        private int sourceBytesPerSample;
        private long length;
        private object lockObject;

        private int fadeSamplePosition;
        private int fadeSampleCount;
        private FadeState fadeState;
        private int fadeInDuration;
        private int fadeOutDuration;
        private int loopFadeInDuration;
        private int loopFadeOutDuration;
        private int loopCount;
        private int currentPlayCount;
        private float sampleVolume;

        /// <summary>
        /// Initializes a new instance of AudioFileReader
        /// </summary>
        /// <param name="fileName">The file to open</param>
        public AudioFileReader(AudioAttr attr)
        {
            this.fileName = attr.fileName;
            this.soundClass = attr.soundClass;

            CreateReaderStream(attr.fileName);
            this.sourceBytesPerSample = (readerStream.WaveFormat.BitsPerSample / 8) * readerStream.WaveFormat.Channels;
            this.destBytesPerSample = 8; // stereo float
            this.sampleChannel = new SampleChannel(readerStream);
            this.length = SourceToDest(readerStream.Length);
            this.lockObject = new object();

            this.fadeInDuration = attr.fadeInMillSecs;
            this.fadeOutDuration = attr.fadeOutMillSecs;
            this.loopFadeInDuration = attr.loopFadeInMillSecs;
            this.loopFadeOutDuration = attr.loopFadeOutMillSecs;

            this.Volume = Utils.computeVolume(attr.sampleVolume);
            this.sampleVolume = attr.sampleVolume;
            this.loopCount = attr.loopCount;
            this.currentPlayCount = 0;   // keep track of how many times the sound has been played (loop support)

            this.fadeState = FadeState.FullVolume;
        }


        /// <summary>
        /// Creates the reader stream, supporting all filetypes in the core NAudio library,
        /// and ensuring we are in PCM format
        /// </summary>
        /// <param name="fileName">File Name</param>
        private void CreateReaderStream(string fileName)
        {
            fileName = AppDomain.CurrentDomain.BaseDirectory + fileName;

            if (fileName.EndsWith(".wav", StringComparison.OrdinalIgnoreCase))
            {
                readerStream = new WaveFileReader(fileName);
                if (readerStream.WaveFormat.Encoding != WaveFormatEncoding.Pcm && readerStream.WaveFormat.Encoding != WaveFormatEncoding.IeeeFloat)
                {
                    readerStream = WaveFormatConversionStream.CreatePcmStream(readerStream);
                    readerStream = new BlockAlignReductionStream(readerStream);
                }
            }
            else if (fileName.EndsWith(".mp3", StringComparison.OrdinalIgnoreCase) || fileName.EndsWith(".sciaudio", StringComparison.OrdinalIgnoreCase))
            {
                readerStream = new Mp3FileReader(fileName);
                Console.WriteLine("Created MP3 file reader");
            }
            else if (fileName.EndsWith(".aiff"))
            {
                readerStream = new AiffFileReader(fileName);
            }
        }

        /// <summary>
        /// WaveFormat of this stream
        /// </summary>
        public override WaveFormat WaveFormat
        {
            get { return sampleChannel.WaveFormat; }
        }

        /// <summary>
        /// Length of this stream (in bytes)
        /// </summary>
        public override long Length
        {
            get { return this.length; }
        }

        /// <summary>
        /// Position of this stream (in bytes)
        /// </summary>
        public override long Position
        {
            get { return SourceToDest(readerStream.Position); }
            set { lock (lockObject) { readerStream.Position = DestToSource(value); } }
        }


        public string FileName
        {
            get { return this.fileName; }
            set { fileName = value; }
        }

        public string SoundClass
        {
            get { return this.soundClass; }
            set { soundClass = value; }
        }

        /// <summary>
        /// Reads from this wave stream
        /// </summary>
        /// <param name="buffer">Audio buffer</param>
        /// <param name="offset">Offset into buffer</param>
        /// <param name="count">Number of bytes required</param>
        /// <returns>Number of bytes read</returns>


        public override int Read(byte[] buffer, int offset, int count)
        {
            WaveBuffer waveBuffer = new WaveBuffer(buffer);
            int samplesRequired = count / 4;
            int samplesRead = Read(waveBuffer.FloatBuffer, offset / 4, samplesRequired);
            return samplesRead * 4;
        }

        /// <summary>
        /// Reads audio from this sample provider
        /// </summary>
        /// <param name="buffer">Sample buffer</param>
        /// <param name="offset">Offset into sample buffer</param>
        /// <param name="count">Number of samples required</param>
        /// <returns>Number of samples read</returns>

        public int Read(float[] buffer, int offset, int count)
        {
            int sourceSamplesRead = 0;

            try
            {
                sourceSamplesRead = this.sampleChannel.Read(buffer, offset, count);

                // looping fade in
                if (sourceSamplesRead == 0 && loopCount != 0 )  // -1 for infinite loop
                {
                    if(loopCount == -1) {
                        Trace.WriteLine("Looping indefinitely");
                    } else {
                        loopCount--;
                        Trace.WriteLine("Looping " + loopCount + " more time(s)");
                    }
                    currentPlayCount++;
                    readerStream.Position = 0;
                    fadeState = FadeState.FullVolume;
                    if (loopFadeInDuration > 0)
                    {
                        this.BeginFadeIn(loopFadeInDuration);
                    }
                }

                // fade in
                if (TimeSpan.FromMilliseconds(fadeInDuration) > readerStream.CurrentTime
                    && currentPlayCount == 0
                    && fadeState != FadeState.FadingIn)
                {
                    this.BeginFadeIn(fadeInDuration);
                }

                // fade out
                if (readerStream.TotalTime.Subtract(TimeSpan.FromMilliseconds(fadeOutDuration)) < readerStream.CurrentTime
                    && !readerStream.TotalTime.Equals(readerStream.CurrentTime)
                    && fadeOutDuration > 0
                    && loopCount == 0
                    && fadeState != FadeState.FadingOut
                  )
                {
                        this.BeginFadeOut(fadeOutDuration);
                }

                // fade out (looping)
                if (readerStream.TotalTime.Subtract(TimeSpan.FromMilliseconds(loopFadeOutDuration)) < readerStream.CurrentTime
                    && !readerStream.TotalTime.Equals(readerStream.CurrentTime)
                    && loopFadeOutDuration > 0
                    && loopCount != 0
                    && fadeState != FadeState.FadingOut
                  )
                {
                        this.BeginFadeOut(loopFadeOutDuration);
                }

                lock (lockObject)
                {
                    if (fadeState == FadeState.FadingIn)
                    {
                        FadeIn(buffer, offset, sourceSamplesRead, Volume);
                    }
                    else if (fadeState == FadeState.FadingOut)
                    {
                        FadeOut(buffer, offset, sourceSamplesRead, Volume);
                    }
                    else if (fadeState == FadeState.Silence)
                    {
                        ClearBuffer(buffer, offset, count);
                    }
                }
            }
            catch
            {
                Trace.WriteLine("Could not get source samples read.");
            }
            return sourceSamplesRead;
        }

        public void BeginFadeIn(double fadeDurationInMilliseconds)
        {
            lock (lockObject)
            {
                fadeSamplePosition = 0;
                fadeSampleCount = (int)((fadeDurationInMilliseconds * sampleChannel.WaveFormat.SampleRate) / 1000);
                fadeState = FadeState.FadingIn;
                Trace.WriteLine("Starting fade in of " + fadeDurationInMilliseconds + " milliseconds");
            }
        }

        public void BeginFadeOut(double fadeDurationInMilliseconds)
        {
            lock (lockObject)
            {
                fadeSamplePosition = 0;
                fadeSampleCount = (int)((fadeDurationInMilliseconds * sampleChannel.WaveFormat.SampleRate) / 1000);
                fadeState = FadeState.FadingOut;
                Trace.WriteLine("Starting fade out of " + fadeDurationInMilliseconds + " milliseconds");
            }
        }

        private void FadeIn(float[] buffer, int offset, int sourceSamplesRead, float volume)
        {
            int sample = 0;
            while (sample < sourceSamplesRead)
            {
                float multiplier = (fadeSamplePosition / (float)fadeSampleCount);
                for (int ch = 0; ch < sampleChannel.WaveFormat.Channels; ch++)
                {
                    buffer[offset + sample++] *= multiplier;
                }
                fadeSamplePosition++;
                if (fadeSamplePosition > fadeSampleCount)
                {
                    fadeState = FadeState.FullVolume;
                    // no need to multiply any more
                    Trace.WriteLine("Fade in complete");
                    break;
                }
            }
        }

        private void FadeOut(float[] buffer, int offset, int sourceSamplesRead, float volume)
        {
            int sample = 0;
            while (sample < sourceSamplesRead)
            {
                float multiplier = 1.0f - (fadeSamplePosition / (float)fadeSampleCount);
                for (int ch = 0; ch < sampleChannel.WaveFormat.Channels; ch++)
                {
                    buffer[offset + sample++] *= multiplier;
                }
                fadeSamplePosition++;
                if (fadeSamplePosition > fadeSampleCount)
                {
                    Trace.WriteLine("Fade out complete");
                    fadeState = FadeState.Silence;
                    // clear out the end
                    ClearBuffer(buffer, sample + offset, sourceSamplesRead - sample);
                    break;
                }
            }
        }

        private static void ClearBuffer(float[] buffer, int offset, int count)
        {
            for (int n = 0; n < count; n++)
            {
                buffer[n + offset] = 0;
            }
        }

        /// <summary>
        /// Gets or Sets the Volume of this AudioFileReader. 1.0f is full volume
        /// </summary>
        public float Volume
        {
            get { return this.sampleChannel.Volume; }
            set { sampleChannel.Volume = value; }
        }

        public float SampleVolume
        {
            get { return this.sampleVolume; }
            set { sampleVolume = value; }
        }

        public int LoopCount
        {
            get { return this.loopCount; }
            set { loopCount = value; }
        }

        public int FadeInDuration
        {
            get { return this.fadeInDuration; }
            set { this.fadeInDuration = value; }
        }

        public int FadeOutDuration
        {
            get { return this.fadeOutDuration; }
            set { this.fadeOutDuration = value; }
        }

        public int LoopFadeInDuration
        {
            get { return this.loopFadeInDuration; }
            set { this.loopFadeInDuration = value; }
        }

        public int LoopFadeOutDuration
        {
            get { return this.loopFadeOutDuration; }
            set { this.loopFadeOutDuration = value; }
        }

        /// <summary>
        /// Helper to convert source to dest bytes
        /// </summary>
        private long SourceToDest(long sourceBytes)
        {
            return destBytesPerSample * (sourceBytes / sourceBytesPerSample);
        }

        /// <summary>
        /// Helper to convert dest to source bytes
        /// </summary>
        private long DestToSource(long destBytes)
        {
            return sourceBytesPerSample * (destBytes / destBytesPerSample);
        }

        /// <summary>
        /// Disposes this AudioFileReader
        /// </summary>
        /// <param name="disposing">True if called from Dispose</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.readerStream.Dispose();
                this.readerStream = null;
            }
            base.Dispose(disposing);
        }
    }


    public class sciAudio {

        static private List<SoundPlayback> playingSounds = new List<SoundPlayback>();
        static bool changedBit = false;
        static string path = AppDomain.CurrentDomain.BaseDirectory;
        static System.Threading.Timer stopTimer;

        public static SoundPlayback FindSoundByFName(String FName)
        {
            foreach (SoundPlayback pb in playingSounds)
            {
                if (pb.audioFileReader.FileName == FName)
                {
                    return (pb);
                }
            }
            return(null);
        }

        private static StreamReader openFile(String filename)
        {
            int retryCount = 20;

            // Open the file
            while (retryCount > 0)
            {
                try
                {
                    return (new StreamReader(filename));
                }
                catch
                {
                    retryCount--;
                    Thread.Sleep(100);
                }
            }
            return (null);
        }

        public static List<AudioAttr> ReadFile(FileSystemEventArgs e)
        {
            List<AudioAttr> audioCommands = new List<AudioAttr>();
            AudioAttr attr = null;
            string line;
            StreamReader r = openFile(path + e.Name);
            while ((line = r.ReadLine()) != null)
            {
                if(line.Trim().ToLower() == "(sciaudio")    // begin command
                {
                    attr = new AudioAttr();
                }
                else if(line.Trim().ToLower() == ")")
                {
                    audioCommands.Add(attr);  // end command
                }
                else {   // attributes
                    Regex regex = new Regex(@"[\w\.\\-]+|""[\w\s\.\\-]*""");

                    var command = regex.Matches(line).Cast<Match>().ToList();

                    if (attr == null)
                    {
                        Trace.WriteLine("Bad file format.  Verify your file starts with '(sciAudio'.  Aborting.");
                        break;
                    }


                    switch (command[0].ToString().Trim().ToLower())
                    {
                        case "command":
                            attr.command = command[1].ToString().ToLower();
                            break;
                        case "filename":
                            attr.fileName = command[1].ToString().ToLower().Replace("\"", "");
                            break;
                        case "soundclass":
                            attr.soundClass = command[1].ToString().ToLower();
                            break;
                        case "volume":
                            try
                            {
                                Trace.WriteLine("attr.command =  " + attr.command); 
                                if(attr.command.Equals("gamevolume"))
                                {
                                   Trace.WriteLine("Current Game Vol = " + Utils.gameVolume);
                                   Utils.gameVolume = Convert.ToInt32(command[1].ToString());
                                   Trace.WriteLine("New Game Vol = " + Utils.gameVolume);
                                } 
                                else
                                {
                                   attr.sampleVolume = Convert.ToInt32(command[1].ToString());
                                   Trace.WriteLine("Sample Volume = " + attr.sampleVolume);
                                }
                            }
                            catch
                            {
                                Trace.WriteLine("Expected integer value for 'volume', provided value was: " + command[1].ToString());
                            }
                            break;
                        case "fadeinmillisecs":
                            try
                            {
                                attr.fadeInMillSecs = Convert.ToInt32(command[1].ToString());
                            }
                            catch
                            {
                                Trace.WriteLine("Expected integer value for 'fadeInMillSecs', provided value was: " + command[1].ToString());
                            }
                            break;
                        case "fadeoutmillisecs":
                            try
                            {
                                attr.fadeOutMillSecs = Convert.ToInt32(command[1].ToString());
                            }
                            catch
                            {
                                Trace.WriteLine("Expected integer value for 'fadeOutMillSecs', provided value was: " + command[1].ToString());
                            }
                            break;
                        case "loopfadeinmillisecs":
                            try
                            {
                                attr.loopFadeInMillSecs = Convert.ToInt32(command[1].ToString());
                            }
                            catch
                            {
                                Trace.WriteLine("Expected integer value for 'loopFadeInMillSecs', provided value was: " + command[1].ToString());
                            }
                            break;
                        case "loopfadeoutmillisecs":
                            try
                            {
                                attr.loopFadeOutMillSecs = Convert.ToInt32(command[1].ToString());
                            }
                            catch
                            {
                                Trace.WriteLine("Expected integer value for 'loopFadeOutMillSecs', provided value was: " + command[1].ToString());
                            }
                            break;
                        case "loopcount":
                            try
                            {
                                attr.loopCount = Convert.ToInt32(command[1].ToString());
                            }
                            catch
                            {
                                Trace.WriteLine("Expected integer value for 'loopCount', provided value was: " + command[1].ToString());
                            }
                            break;
                        default:
                            Trace.WriteLine("Unrecognized parameter supplied, full line is: " + line.ToString());
                            break;
                    }
                }
            }
            r.Close();
            r.Dispose();

            return audioCommands;
        }

      
        public static void StopandDestroy(Object o)
        {
            SoundPlayback soundPlayback = (SoundPlayback)o;
            Trace.WriteLine("Stopping playback & disposing");
            try
            {
                soundPlayback.playbackDevice.Stop();
                soundPlayback.playbackDevice.Dispose();
            }
            catch (Exception e)
            {
                Trace.WriteLine("Cound not stop and dispose!" + e.ToString());
            }
            Trace.WriteLine("Disposing of file stream");
            try
            {
                soundPlayback.audioFileReader.Dispose();
            }
            catch (Exception e)
            {
                Trace.WriteLine("Could not dispose!" + e.ToString());
            }

        }


        private static void playCommand(AudioAttr attr)
        {
            SoundPlayback soundPlayback = new SoundPlayback(attr);
            try
            {
                soundPlayback.playbackDevice.Play();
                playingSounds.Add(soundPlayback);
                Trace.WriteLine("Beginning playback for: " + attr.fileName);
                Trace.WriteLine("sample volume is: " + soundPlayback.audioFileReader.SampleVolume);
            }
            catch (Exception e)
            {
                Trace.WriteLine("Could not initialize playback." + e.ToString()); 
            }
        }

        private static void stopCommand(AudioAttr attr)
        {

            if (!attr.soundClass.Equals("noClass"))
            {
                if (playingSounds == null || playingSounds.Count() == 0)
                {
                    Trace.WriteLine("Sound class isn't currently playing, cannot stop: " + attr.soundClass);
                    return;
                }

                Trace.WriteLine("Stopping playback for sound class: " + attr.soundClass);
                foreach (SoundPlayback soundPlayback in playingSounds)
                {
                    if (soundPlayback.audioFileReader.SoundClass == attr.soundClass || attr.soundClass.ToUpper() == "ALL")
                    {
                        // fade
                        if (attr.fadeOutMillSecs > 0)
                        {
                            soundPlayback.audioFileReader.BeginFadeOut(attr.fadeOutMillSecs);
                        }
                        else // stop it now 
                        {
                            StopandDestroy(soundPlayback);
                        }

                    }
                }
                // remove the sounds from the collection
                for (int i = playingSounds.Count - 1; i >= 0; i--)
                {
                    if (playingSounds[i].audioFileReader.SoundClass == attr.soundClass || attr.soundClass.ToUpper() == "ALL")
                    {
                        playingSounds.RemoveAt(i);
                    }
                }
            // stop filename
            } else {
                SoundPlayback soundPlayback = sciAudio.FindSoundByFName(attr.fileName);

                // didn't find a playing sound
                if (soundPlayback == null)
                {
                    Trace.WriteLine("File isn't currently playing, cannot stop: " + attr.fileName);
                    return;
                }

                // fade
                if (attr.fadeOutMillSecs > 0)
                {
                    soundPlayback.audioFileReader.BeginFadeOut(attr.fadeOutMillSecs);
                    stopTimer = new System.Threading.Timer(new TimerCallback(StopandDestroy), soundPlayback, attr.fadeOutMillSecs, -1);
                }
                else  // stop it now
                {
                    StopandDestroy(soundPlayback);
                }
                playingSounds.Remove(soundPlayback);
            }

        }


    
        private static void changeCommand(AudioAttr attr)
        {

           if (!attr.soundClass.Equals("noClass"))
             {
                if (playingSounds == null || playingSounds.Count() == 0)
                {
                    Trace.WriteLine("Sound class isn't currently playing, cannot change playback for " + attr.soundClass);
                    return;
                }

                Trace.WriteLine("Changing playback for sound class:" + attr.soundClass);
                foreach (SoundPlayback soundPlayback in playingSounds)
                {
                    if (soundPlayback.audioFileReader.SoundClass == attr.soundClass || attr.soundClass.ToUpper() == "ALL")
                    {
                        Trace.WriteLine("Changing playback for " + soundPlayback.audioFileReader.FileName);
                        soundPlayback.audioFileReader.Volume = Utils.computeVolume(attr.sampleVolume);
                        soundPlayback.audioFileReader.SoundClass = attr.soundClass;
                        soundPlayback.audioFileReader.LoopFadeInDuration = attr.loopFadeInMillSecs;
                        soundPlayback.audioFileReader.LoopFadeOutDuration = attr.loopFadeOutMillSecs;
                        soundPlayback.audioFileReader.LoopCount = attr.loopCount;
                    }
                }
            } else {
           
                SoundPlayback soundPlayback = sciAudio.FindSoundByFName(attr.fileName);

                // didn't find a playing sound
                if (soundPlayback == null)
                {
                    Trace.WriteLine("File isn't currently playing, cannot change volume: " + attr.fileName);
                    return;
                }
                else
                {
                    Trace.WriteLine("Changing playback for " + attr.fileName);
                    soundPlayback.audioFileReader.Volume = Utils.computeVolume(attr.sampleVolume);
                    soundPlayback.audioFileReader.SoundClass = attr.soundClass;
                    soundPlayback.audioFileReader.LoopFadeInDuration = attr.loopFadeInMillSecs;
                    soundPlayback.audioFileReader.LoopFadeOutDuration = attr.loopFadeOutMillSecs;
                    soundPlayback.audioFileReader.LoopCount = attr.loopCount;
                }
            }
        }


        private static void gameVolumeCommand(AudioAttr attr)
        {
            foreach (SoundPlayback soundPlayback in playingSounds)
            {
                soundPlayback.audioFileReader.Volume = Utils.computeVolume(soundPlayback.audioFileReader.SampleVolume);
            }
        }

        private static void OnChangedConductor(object source, FileSystemEventArgs e)
        {
            changedBit = !changedBit;
            if (changedBit)
            {
                List<AudioAttr> audioCommands = new List<AudioAttr>();
                audioCommands = ReadFile(e);

                // loop through each command in file
                foreach (AudioAttr audioCmd in audioCommands)
                {
                    switch (audioCmd.command)
                    {
                        case "play":
                            playCommand(audioCmd);
                            break;
                        case "stop":
                            stopCommand(audioCmd);
                            break;
                        case "change":
                            changeCommand(audioCmd);
                            break;
                        case "gamevolume":
                            gameVolumeCommand(audioCmd);
                            break;
                        case "exit":
                            Exit();
                            break;
                        default:
                            Trace.WriteLine("Valid values for command are: play|stop|change|gamevolume|exit");
                            break;
                    }

                }

            }
        }

        public class SoundPlayback
        {
            public IWavePlayer playbackDevice;
            public AudioFileReader audioFileReader;

            public SoundPlayback(AudioAttr attr)
            {
                int retryCount = 10; 
                while (retryCount > 0)
                {
                    try
                    {
                        audioFileReader = new AudioFileReader(attr);
                        playbackDevice = new WaveOutEvent();
                        playbackDevice.Init(audioFileReader);
                        retryCount--;
                    }
                    catch (Exception e)
                    {
                        Trace.WriteLine(e.ToString());
                        Trace.WriteLine("...will retry to play this sound " + retryCount + " more times");
                    }
                }
            }

        }

        static void SetupTrace()
        {

            // Set up trace file
            Trace.Listeners.Clear();
            Stream myFile = File.Create(AppDomain.CurrentDomain.BaseDirectory + "sciAudio.log");

            TextWriterTraceListener twtl = new TextWriterTraceListener(myFile, AppDomain.CurrentDomain.FriendlyName);
            twtl.Name = "TextLogger";
            twtl.TraceOutputOptions = TraceOptions.ThreadId | TraceOptions.DateTime;

            ConsoleTraceListener ctl = new ConsoleTraceListener(false);
            ctl.TraceOutputOptions = TraceOptions.DateTime;

            Trace.Listeners.Add(twtl);
            Trace.Listeners.Add(ctl);
            Trace.AutoFlush = true;
        }

        static void Exit()
        {
            // Close all open sounds on exit
            foreach (SoundPlayback soundPlayback in playingSounds)
            {
                soundPlayback.playbackDevice.Dispose();
                soundPlayback.audioFileReader.Dispose();
            }
            Trace.WriteLine("Monitored application not found running, or the 'exit' command was issued.  Exiting application.");

            Trace.Close();
            Environment.Exit(0);
        }

        static void Main(string[] args)
        {

            SetupTrace();
            Trace.WriteLine("sciAudio by Jeremiah Nellis");
            Trace.WriteLine("  v.1.0 - Created Aug 17th 2012.");
            Trace.WriteLine("  v.1.1 - Updated Mar 11th 2015.");
            Trace.WriteLine("  v.1.2 - Updated Jul 1st 2020.");
            Trace.WriteLine("  v.1.2.1 - Updated Jul 11th 2020.");

            FileSystemWatcher watch = new FileSystemWatcher();

            watch.Path = path;
            watch.IncludeSubdirectories = true;

            watch.NotifyFilter = NotifyFilters.LastWrite;

            // Only watch the 'conductor' files.
            watch.Filter = "*.con";

            Trace.WriteLine("Monitoring all " + watch.Filter + " files in " + watch.Path);

            watch.Changed += new FileSystemEventHandler(OnChangedConductor);
            watch.EnableRaisingEvents = true;

            Thread.Sleep(5000);    // Wait 5 seconds to allow the game application to crank up

            // 01/24/2015 JNellis:  Instead of looping, just block the main thread and wait for the process to complete
            // Attach to the game, grabbing the most recent process started within the past 30 seconds
            string iniFile = watch.Path + "sciAudio.ini";
            Trace.WriteLine("Reading INI file: " + iniFile);
            var MyIni = new IniFile(iniFile);

            string GameExecutableName1 = MyIni.Read("GameExecutableName1");
            string GameExecutableName2 = MyIni.Read("GameExecutableName2");
            string GameExecutableName3 = MyIni.Read("GameExecutableName3");
            string GameExecutableName4 = MyIni.Read("GameExecutableName4");
            string GameExecutableName5 = MyIni.Read("GameExecutableName5");

            Process p = Process.GetProcessesByName(GameExecutableName1).Where(n => n.StartTime >= DateTime.Now.AddSeconds(-30)).OrderByDescending(n => n.StartTime).FirstOrDefault();
            if (p != null)
            {
                Trace.WriteLine("Monitoring application: " + GameExecutableName1 + ". sciAudio application will run until " + GameExecutableName1 +  " exits.");
            }

            if(p == null)
            {
                p = Process.GetProcessesByName(GameExecutableName2).Where(n => n.StartTime >= DateTime.Now.AddSeconds(-30)).OrderByDescending(n => n.StartTime).FirstOrDefault();
                if (p != null)
                {
                    Trace.WriteLine("Monitoring application: " + GameExecutableName2 + ". sciAudio application will run until " + GameExecutableName2 + " exits.");
                }
            }

            if (p == null)
            {
                p = Process.GetProcessesByName(GameExecutableName3).Where(n => n.StartTime >= DateTime.Now.AddSeconds(-30)).OrderByDescending(n => n.StartTime).FirstOrDefault();
                if (p != null)
                {
                    Trace.WriteLine("Monitoring application: " + GameExecutableName3 + ". sciAudio application will run until " + GameExecutableName3 + " exits.");
                }
            }

            if (p == null)
            {
                p = Process.GetProcessesByName(GameExecutableName4).Where(n => n.StartTime >= DateTime.Now.AddSeconds(-30)).OrderByDescending(n => n.StartTime).FirstOrDefault();
                if (p != null)
                {
                    Trace.WriteLine("Monitoring application: " + GameExecutableName4 + ". sciAudio application will run until " + GameExecutableName4 + " exits.");
                }
            }

            if (p == null)
            {
                p = Process.GetProcessesByName(GameExecutableName5).Where(n => n.StartTime >= DateTime.Now.AddSeconds(-30)).OrderByDescending(n => n.StartTime).FirstOrDefault();
                if (p != null)
                {
                    Trace.WriteLine("Monitoring application: " + GameExecutableName5 + ". sciAudio application will run until " + GameExecutableName5 + " exits.");
                }
            }

            // game process is running, wait until it exits then kill everything
            if (p != null)
            {
                p.WaitForExit();
            }
            else
            {
                Trace.WriteLine("Something went wrong.  No possible monitored applications found running, exiting.  Review the monitored applications specified in sciAudio.ini file.");
            }

            Exit();
        }
    }
    
}
